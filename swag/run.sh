#!/usr/bin/env bash

# subdomains
# yt - invidious
# oc - swag index.html page
# tube - lighttube
# calendar - caldav calendar
# search - searxng
# x - nitter

# extra domains
# ci.r0bot.org - woodpecker ci

CONFIG_DIR="$(pwd)/config"

docker run -d \
  --name=swag \
  --cap-add=NET_ADMIN \
  --net=swag-containers \
  --env PUID=1000 \
  --env PGID=1000 \
  --env TZ=Etc/UTC \
  --env URL=sheev.net \
  --env VALIDATION=http \
  --env SUBDOMAINS=oc,search,x \
  --env CERTPROVIDER= `#optional` \
  --env DNSPLUGIN=cloudflare `#optional` \
  --env PROPAGATION= `#optional` \
  --env EMAIL=keb@sheev.net \
  --env ONLY_SUBDOMAINS=true `#optional` \
  --env EXTRA_DOMAINS=ci.r0bot.org \
  --env STAGING=false `#optional` \
  --publish 443:443 \
  --publish 80:80 `#optional` \
  --volume $CONFIG_DIR:/config \
  --restart unless-stopped \
  lscr.io/linuxserver/swag:latest

# let you actually edit the files and folders created by SWAG
sudo chown -hR $(id -u):$(id -g) $CONFIG_DIR
